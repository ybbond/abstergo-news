// @flow

import React from 'react';
// import logo from './logo.svg';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import './App.css';

import HomeScene from './scenes/HomeScene';
import ProfileScene from './scenes/ProfileScene';

function App() {
  return (
    <Router>
      <Route path="/" exact component={HomeScene} />
      <Route path="/user" component={ProfileScene} />
    </Router>
  );
}

export default App;
