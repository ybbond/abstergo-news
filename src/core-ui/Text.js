// @flow

import {type ComponentType} from 'react';
import styled from 'styled-components';

type Props = {
  bold?: boolean,
  italic?: boolean,
};

const Text: ComponentType<Props> = styled.p`
  font-weight: ${props => (props.bold ? 700 : 400)};
  font-style: ${props => (props.italic ? 'italic' : 'normal')};
  margin: 0;
`;

export default Text;
