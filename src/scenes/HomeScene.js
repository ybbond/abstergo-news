// @flow

import React from 'react';

import {
  AppWrapper,
  ArticlePreview,
  Toolbar,
  FloatingSearch,
} from '../components';

const HomeScene = () => (
  <>
    <Toolbar />
    <AppWrapper>
      <ArticlePreview />
      <ArticlePreview />
      <ArticlePreview />
      <ArticlePreview />
      <ArticlePreview />
      <ArticlePreview />
      <ArticlePreview />
      <ArticlePreview />
    </AppWrapper>
    <FloatingSearch onOpenSearch={() => {}} />
  </>
);

export default HomeScene;
