// @flow

const MOCK_CONTENT = [
  {
    title:
      'What If an Algorithm Could Predict Your Unborn Child’s Intelligence?',
    subtitle:
      'Stephen Hsu’s startup Genomic Predictions analyzes genetic data to predict the chance of diseases like diabetes and cancer — and forecast IQ',
    author: 'Oscar Schwartz',
    image:
      'https://cdn-images-1.medium.com/focal/2600/780/49/44/1*j4dQbwuU-zcx8QBeUu8hKg.jpeg',
  },
  {
    title: 'How to Get Started With React Hooks: Controlled Forms',
    subtitle: '',
    author: 'Ovie Okeh',
    image: 'https://miro.medium.com/max/1400/1*0MgGEfZfLO91g1Oa2h3ebQ@2x.png',
  },
  {
    title: 'How ‘Digital Nomads’ Game the System to Work in Paradise',
    subtitle:
      'Visa scams and tax evasion are common as locals struggle with a low minimum wage',
    author: 'Harry Guinness',
    image:
      'https://cdn-images-1.medium.com/focal/1600/480/50/34/1*fa53SNj4lAnNpnPV30C-pA.jpeg',
  },
  {
    title: 'How to update the Apollo Client’s cache after a mutation',
    subtitle: 'The Apollo Client and its cache',
    author: 'ric0',
    image: 'https://miro.medium.com/max/1400/1*CbevjJN6IQBk7gbh38V-2g.png',
  },
];

export default MOCK_CONTENT;
