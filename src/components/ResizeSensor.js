// @flow

import React, {createContext} from 'react';

const WindowWidthContext = createContext();
