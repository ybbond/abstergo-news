// @flow

import React from 'react';
import styled from 'styled-components';
import abstergo_logo from '../assets/abstergo_logo.png';
import search_logo from '../assets/logo_search.png';

import {Text} from '../core-ui';
import COLORS from '../constants/COLORS';

const ToolbarBody = styled.div`
  align-items: center;
  background-color: ${COLORS.MAIN};
  flex-direction: row;
  min-height: 50px;
  width: 100%;
  display: flex;
  position: fixed;
  left: 0px;
  top: 0px;
  z-index: 10000;
`;

const AbstergoLogo = styled.img`
  width: 26px;
  height: 26px;
`;

const SearchIcon = styled.img`
  margin-right: 25px;
  width: 24px;
  height: 26px;
`;

const MockProfilePic = styled.div`
  background-color: grey;
  margin-right: 40px;
  border-radius: 15px;
  height: 30px;
  width: 30px;
`;

const ToolbarTitle = styled(Text)`
  margin-left: 10px;
  color: ${COLORS.WHITE};
  font-size: 16px;
`;

const AbstergoContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  margin-left: 20px;

  @media (min-width: 666px) {
    flex: 1;
    margin-left: 119px;
  }
`;

const RightMenu = styled.div`
  display: flex;
  flex-direction: row;
  @media (max-width: 666px) {
    display: none;
  }
`;

const Abstergo = () => (
  <AbstergoContainer>
    <AbstergoLogo src={abstergo_logo} />
    <ToolbarTitle>Abstergo</ToolbarTitle>
  </AbstergoContainer>
);

const Navbar = () => (
  <ToolbarBody>
    <Abstergo />
    <RightMenu>
      <SearchIcon src={search_logo} />
      <MockProfilePic />
    </RightMenu>
  </ToolbarBody>
);

export default Navbar;
