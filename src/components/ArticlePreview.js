// @flow

import React from 'react';
import styled from 'styled-components';

import MOCK_CONTENT from './MOCK_ARTICLE';
import {Text} from '../core-ui';

const Container = styled.div`
  display: flex;
  flex-direction: row;

  @media (max-width: 666px) {
    margin-bottom: 24px;
  }
`;

const LeftDiv = styled.div`
  flex: 2;

  @media (max-width: 666px) {
    flex: 1;
  }
`;

const RightDiv = styled.div`
  flex: 3;
  flex-direction: column;
  margin-left: 10px;
`;

const ArticleImage = styled.img`
  height: 160px;
  width: 100%;
  object-fit: cover;

  @media (max-width: 666px) {
    height: 160px;
    width: 160px;
  }

  @media (max-width: 412px) {
    height: 80px;
    width: 80px;
  }
`;

const ArticleTitle = styled(Text)`
  font-size: 20px;
  line-height: 20px;
  font-weight: 600;

  @media (max-width: 666px) {
    font-size: 16px;
  }
`;

const ArticleSubtitle = styled(Text)`
  font-size: 16px;
  line-height: 20px;

  @media (max-width: 666px) {
    font-size: 14px;
    text-overflow: ellipsis;
    overflow: hidden;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
  }
`;

const AuthorAndDate = styled(Text)`
  margin-top: 10px;
  font-size: 13px;
`;
const AuthorBold = styled.span`
  font-weight: 600;
`;

const ArticlePreview = () => {
  const index =
    Math.random() > 0.5
      ? 0
      : Math.random() > 0.5
      ? 1
      : Math.random() > 0.5
      ? 2
      : 3;
  return (
    <Container>
      <LeftDiv>
        <ArticleImage src={MOCK_CONTENT[index].image} />
      </LeftDiv>
      <RightDiv>
        <ArticleTitle>{MOCK_CONTENT[index].title}</ArticleTitle>
        <ArticleSubtitle>{MOCK_CONTENT[index].subtitle}</ArticleSubtitle>
        <AuthorAndDate>
          <AuthorBold>{MOCK_CONTENT[index].author} ·</AuthorBold> Jul 9
        </AuthorAndDate>
      </RightDiv>
    </Container>
  );
};

export default ArticlePreview;
