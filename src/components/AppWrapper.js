// @flow

import styled from 'styled-components';

const AppWrapper = styled.div`
  min-height: 100vh;
  max-width: 666px;
  display: flex;
  flex: 1 1 auto;
  flex-direction: column;
  align-items: left;
  justify-content: center;
  font-size: calc(10px + 2vmin);
  margin: 0 auto 0;
  padding: 0px;
  padding-top: 60px;

  @media (max-width: 666px) {
    padding: 10px;
    padding-top: 60px;
  }
`;

export default AppWrapper;
