// @flow

import React from 'react';
import styled from 'styled-components';
import search_logo from '../assets/logo_search.png';
import close_logo from '../assets/logo_cross.png';

import COLORS from '../constants/COLORS';

const FloatingContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: ${COLORS.MAIN};
  position: fixed;
  bottom: 50px;
  right: 30px;
  border-radius: 22px;
  width: 44px;
  height: 44px;

  @media (min-width: 666px) {
    display: none;
  }
`;

const SearchLogo = styled.img`
  width: 24px;
  height: 26px;
`;

const CloseLogo = styled.img`
  width: 26px;
  height: 26px;
`;

type Props = {
  onCloseSearch?: () => void,
  onOpenSearch?: () => void,
};

const FloatingSearch = (props: Props) => {
  const {onCloseSearch, onOpenSearch} = props;
  return (
    <FloatingContainer onClick={onOpenSearch ? onOpenSearch : onCloseSearch}>
      {onOpenSearch ? (
        <SearchLogo src={search_logo} />
      ) : (
        <CloseLogo src={close_logo} />
      )}
    </FloatingContainer>
  );
};

export default FloatingSearch;
