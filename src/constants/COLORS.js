// @flow

const COLORS = {
  MAIN: '#2c2c2c',
  WHITE: '#ffffff',
};

export default COLORS;
